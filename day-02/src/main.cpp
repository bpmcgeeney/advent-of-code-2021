#include <fstream>
#include <iostream>
#include <string>
#include <vector>

void part1() { // Part 1
  int horizontalPosition = 0;
  int depth = 0;
  std::string word;

  // File IO
  std::ifstream file;
  file.open("input.txt");

  // Process File
  while (file >> word) {
    if (word == "forward") {
      file >> word;
      horizontalPosition += stoi(word);

    } else if (word == "up") {
      file >> word;
      depth -= stoi(word);

    } else if (word == "down") {
      file >> word;
      depth += stoi(word);
    }
  }

  std::cout << "Part 1: " << horizontalPosition * depth << "\n";
}

void part2() {
  int horizontalPosition = 0;
  int depth = 0;
  int aim = 0;
  std::string word;

  // File IO
  std::ifstream file;
  file.open("input.txt");

  // Process File
  while (file >> word) {
    if (word == "forward") {
      file >> word;
      horizontalPosition += stoi(word);
      depth += (stoi(word) * aim);

    } else if (word == "up") {
      file >> word;
      aim -= stoi(word);

    } else if (word == "down") {
      file >> word;
      aim += stoi(word);
    }
  }

  std::cout << "Part 2: " << horizontalPosition * depth << "\n";
  
}

int main() {
  part1();
  part2();

  return 0;
}
