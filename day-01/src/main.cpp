#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int main()
{
  // File IO
  std::vector<int> depthValues;
  std::string line;
  std::ifstream sonarInput("input.txt");

  // Part 1
  int i = 0;     // loop count
  int part1 = 0; // positive delta count

  for (int depthValue; std::getline(sonarInput, line); depthValue = std::stoi(line))
  {
    depthValues.push_back(depthValue);
    if ((depthValues[i] > depthValues[i - 1]) && (i != 0))
    {
      part1++;
    }

    i++;
  }

  // Part 2
  int part2 = 0;
  int A, B = 0;

  for (int j = 0; j < int(depthValues.size()); j++)
  {
    A = depthValues[j - 3] + depthValues[j - 2] + depthValues[j - 1];
    B = depthValues[j] + depthValues[j - 1] + depthValues[j - 2];

    if (B > A)
    {
      part2++;
    }
  }

  // Print
  std::cout << " Part 1: " << part1 << std::endl;
  std::cout << " Part 2: " << part2 << std::endl;

  return 0;
}
