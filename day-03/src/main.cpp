#include <fstream>
#include <iostream>
#include <math.h>

int main() {
  int gamma = 0;
  int epsilon = 0;
  bool gamma_arr[12] = {0};
  bool epsilon_arr[12] = {0};

  int count1[12] = {0};
  int count0[12] = {0};
  int column = 0;
  char c = ' ';

  // File IO
  std::ifstream file;
  std::string bin;
  file.open("input.txt");

  // Read File
  while (file.get(c)) {

    if (column == 12) {
      column = -1;
    } else if (c == '0') {
      ++count0[column];
    } else if (c == '1') {
      ++count1[column];
    }
    column++;
  }

  for (int i = 0; i < 12; i++) {
    // Compare bits
    if (count1[i] > count0[i]) {
      gamma_arr[i] = 1;
      epsilon_arr[i] = 0;
    } else {
      gamma_arr[i] = 0;
      epsilon_arr[i] = 1;
    }
    
    // Convert to decimal
    gamma += gamma_arr[i] * pow(2, 11 - i);
    epsilon += epsilon_arr[i] * pow(2, 11 - i);
  }

  // Print and close file
  std::cout << "Gamma: " << gamma << std::endl;
  std::cout << "Epsilon: " << epsilon << std::endl;
  std::cout << "Gamma * Epsilon: " << gamma * epsilon << std::endl;
  file.close();
 
   return 0;
}
